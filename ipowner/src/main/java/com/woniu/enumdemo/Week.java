package com.woniu.enumdemo;

public enum Week {
//    SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY;
//
//    Week(){
//        System.out.println("hello");
//    }

//    SUNDAY(7), MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6);
//
//    Week(int weekNum){
//        System.out.println(weekNum);
//    }


//    SUNDAY(7), MONDAY(1), TUESDAY(2), WEDNESDAY(3), THURSDAY(4), FRIDAY(5), SATURDAY(6);
//
//    private int weekNum;
//
//    Week(int weekNum){
//        this.weekNum = weekNum;
//    }
//
//    public int getWeekNum() {
//        return weekNum;
//    }


    SUNDAY(){
        @Override
        public void getWeekNum() {
            System.out.println(7);
        }
    },
    MONDAY() {
        @Override
        public void getWeekNum() {
            System.out.println("星期一");
        }
    },

    TUESDAY(){
        @Override
        public void getWeekNum() {
            System.out.println("礼拜二");
        }
    },
    WEDNESDAY(){
        @Override
        public void getWeekNum() {
            System.out.println("周三");
        }
    };

    public abstract void getWeekNum();




}
