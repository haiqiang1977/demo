package com.woniu.interceptor;

import com.woniu.ip.AddressUtils;
import com.woniu.ip.IPUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * IP 归属地获取，一个依赖轻松搞定
 */
@Slf4j
@Configuration
public class IpUrlLimitInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        //通过本地获取
//        获得ip
        String ip = IPUtil.getIpAddr(httpServletRequest);
//        解析具体地址
        String addr = IPUtil.getAddr("2.58.179.255");

        //通过在线库获取
        String ip2 = IPUtil.getIpAddr(httpServletRequest);
        String ipaddr2 = AddressUtils.getRealAddressByIP(ip2);
        log.info("IP >> {},Address >> {}", ip2, 2);
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {

    }
}
