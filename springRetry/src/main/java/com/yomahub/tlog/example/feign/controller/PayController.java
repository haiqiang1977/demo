package com.yomahub.tlog.example.feign.controller;


import com.yomahub.tlog.example.feign.service.TestRetryService;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class PayController {


    @Autowired
    TestRetryService testRetryService;

    @GetMapping("/addCode/{code}")
    public String addCode(@PathVariable int code) throws Exception {
        return testRetryService.test(code);
    }


}
