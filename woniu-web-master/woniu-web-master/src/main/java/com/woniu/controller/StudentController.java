package com.woniu.controller;

import com.woniu.entity.Student;
import com.woniu.service.StudentService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 学生表(Student)表控制层
 */
@RestController
@RequestMapping("/student")
public class StudentController {
    /**
     * 服务对象
     */
    @Resource
    private StudentService studentService;


    @GetMapping("register")
    public void register(Student student) {
        this.studentService.register(student);
    }


    @GetMapping("/getByPhoneNumber")
    public List<Student> getByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber) {
        return studentService.getByPhoneNumber(phoneNumber);
    }


}
