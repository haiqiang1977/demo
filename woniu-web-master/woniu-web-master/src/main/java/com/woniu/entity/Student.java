package com.woniu.entity;

import lombok.Data;
import java.io.Serializable;

/**
 * 学生表(Student)实体类
 */
@Data
public class Student implements Serializable {

    private Integer id;

    private String name;

    private String phoneNumber;

}
