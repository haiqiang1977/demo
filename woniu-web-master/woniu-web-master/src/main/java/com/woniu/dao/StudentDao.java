package com.woniu.dao;

import com.woniu.entity.Student;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 学生表(Student)表数据库访问层
 *
 */
@Mapper
public interface StudentDao {


    void insertPhoneKeyworkds(@Param("persondId") int persondId, @Param("phoneKeywords") String phoneKeywords);


    void insert(@Param("student") Student student);

    List<Student> getByPhoneNumber(@Param("phoneNumberEncrypt") String phoneNumberEncrypt);
}
