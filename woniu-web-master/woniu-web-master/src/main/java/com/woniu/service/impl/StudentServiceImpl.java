package com.woniu.service.impl;

import com.woniu.config.PassWordDeal;
import com.woniu.dao.StudentDao;
import com.woniu.entity.Student;
import com.woniu.service.StudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 学生表(Student)表服务实现类
 */
@Service("studentService")
public class StudentServiceImpl implements StudentService {
    @Resource
    private StudentDao studentDao;

    @Resource
    PassWordDeal passWordDeal;


    @Override
    public Student insert(Student student) {
        this.studentDao.insert(student);
        return student;
    }

    @Override
    public boolean queryStuCodeRepeat(String stuCode) {
        return false;
    }

    @Override
    public void register(Student student) {
        passWordDeal.register(student);
    }

    @Override
    public List<Student> getByPhoneNumber(String phoneNumber) {
        //先加密
        String phoneNumberEncrypt = passWordDeal.encrypt(phoneNumber);

        return studentDao.getByPhoneNumber(phoneNumberEncrypt);
    }


}
