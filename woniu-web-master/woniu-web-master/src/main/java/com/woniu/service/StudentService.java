package com.woniu.service;

import com.woniu.entity.Student;
import java.util.List;

/**
 * 学生表(Student)表服务接口
 */
public interface StudentService {

    /**
     * 新增数据
     *
     * @param student 实例对象
     * @return 实例对象
     */
    Student insert(Student student);


     boolean queryStuCodeRepeat(String stuCode);

    void register(Student student);

    List<Student> getByPhoneNumber(String phoneNumber);
}
