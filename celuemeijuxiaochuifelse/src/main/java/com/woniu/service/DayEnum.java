package com.woniu.service;

/**
 * @className: DayEnum
 * @author: woniu
 * @date: 2023/3/20
 **/
public enum DayEnum {

    Monday {
        @Override
        public String toDo() {
//            ......省略复杂语句
            return "今天上英语课";
        }
    },
    Tuesday {
        @Override
        public String toDo() {
            return "今天上语文课";
        }
    },
    Wednesday {
        @Override
        public String toDo() {
            return "今天上数学课";
        }
    },
    Thursday {
        @Override
        public String toDo() {
            return "今天上音乐课";
        }
    };
    public abstract String toDo();
}
