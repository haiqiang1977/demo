package com.woniu.service.controller;

import com.alibaba.fastjson.JSONObject;
import com.woniu.service.entity.Order;
import com.woniu.service.service.IOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * SpringBoot+ShardingSphere实现数据库字段加密存储和解密返回
 */
@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {


    @Autowired
    private IOrderService orderService;


    /**
     * 查询订单
     */
    @GetMapping("/list")
    public ResponseEntity<List<Order>> list() {
        return ResponseEntity.ok().body(orderService.list());
    }

    /**
     * 插入订单
     */
    @PostMapping("/save")
    public ResponseEntity<List<Order>> save(@RequestBody Order order) {
        order.setCreatedAt(new Date());
        order.setUpdatedAt(new Date());
        boolean ret = orderService.save(order);
        return ret ? ResponseEntity.ok().body(orderService.list())
                : ResponseEntity.badRequest().body(new ArrayList<>());
    }
}
