package com.woniu.service.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * <p>
 *  状态枚举类
 * </p>
 *
 * @author 公众号：【程序员蜗牛g】
 */
public class CodeGenerator {

	private static final String url = "jdbc:mysql://localhost:3306/encrypt_demo?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf8&useSSL=false";
	private static final String username = "root";
	private static final String password = "123456";
	private static final String outputDir = "D:\\workspace\\workspace_blog\\encrypt-demo\\src\\main\\java"; // entity、mapper、service、controller生成的目录地址，换成自己项目的。
	private static final String xmlOutputDir = "D:\\workspace\\workspace_blog\\encrypt-demo\\src\\main\\resources\\mapper"; // xxMapper.xml生成的目录地址，换成自己项目的。

	public static void main(String[] args) {
		FastAutoGenerator.create(url, username, password)
				.globalConfig(builder -> {
					builder.author("公众号：【程序员蜗牛g】") // 设置作者
							.enableSwagger() // 开启 swagger 模式
							.fileOverride() // 覆盖已生成文件
							.outputDir(outputDir); // 指定输出目录
				})
				.packageConfig(builder -> {
					builder.parent("com.example.encrypt") // 设置父包名，和自己项目的父包名一致即可。
							.moduleName("") // 设置父包模块名，为空就会直接生成在父包名目录下。
							.pathInfo(Collections.singletonMap(OutputFile.mapperXml, xmlOutputDir)); // 设置mapperXml生成路径
				})
				.strategyConfig(builder -> {
					builder.addInclude("tb_order") // 设置需要生成的表名，多个用逗号隔开。
							.addTablePrefix("t_", "tb_", "c_"); // 设置过滤表前缀，多个用逗号隔开。
				})
				.templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
				.execute();
	}

}
